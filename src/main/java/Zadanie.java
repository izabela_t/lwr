import java.util.List;

public class Zadanie {
    String id;
    Integer r;
    List<Element> marszruta;

    public Zadanie(String id, Integer r, List<Element> marszruta){
        this.id = id;
        this.r = r;
        this.marszruta = marszruta;

        for (Element m : this.marszruta) {
            m.zadanie = this;
        }
    }

    public Integer liczPozostalyCzas(){
        return marszruta
                .stream()
                .filter(m -> m.stan == Element.Stan.OCZEKUJE)
                .mapToInt(m -> m.czasTrwania)
                .sum();
    }

    @Override
    public String toString() {
        return "Zadanie{" +
                "id='" + id + '\'' +
                ", r=" + r +
                ", marszruta=" + marszruta +
                '}';
    }
}
