import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ZadanieWTabeli {
    private IntegerProperty nr;
    public void setNr(Integer value) { nrProperty().set(value); }
    public Integer getNr() { return nrProperty().get(); }
    public IntegerProperty nrProperty() {
        if (nr == null) nr = new SimpleIntegerProperty(this, "nr");
        return nr;
    }

    private IntegerProperty r;
    public void setR(Integer value) { rProperty().set(value); }
    public Integer getR() { return rProperty().get(); }
    public IntegerProperty rProperty() {
        if (r == null) r = new SimpleIntegerProperty(this, "r");
        return r;
    }

    private StringProperty marszruta;
    public void setMarszruta(String value) { marszrutaProperty().set(value); }
    public String getMarszruta() { return marszrutaProperty().get(); }
    public StringProperty marszrutaProperty() {
        if (marszruta == null) marszruta = new SimpleStringProperty(this, "marszruta");
        return marszruta;
    }

    private StringProperty kolor;
    public void setKolor(String value) { kolorProperty().set(value); }
    public String getKolor() { return kolorProperty().get(); }
    public StringProperty kolorProperty() {
        if (kolor == null) kolor = new SimpleStringProperty(this, "kolor");
        return kolor;
    }

    public ZadanieWTabeli(Integer nr, Integer r, String marszruta, String kolor) {
        setNr(nr);
        setR(r);
        setMarszruta(marszruta);
        setKolor(kolor);
    }
}