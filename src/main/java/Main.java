import javafx.application.Application;

import java.util.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.stream.Collectors;

public class Main extends Application {
    StackedBarChart<Number, String> stackedBarChart;
    Map<String, String> mapaKolorow = new HashMap<>();
    List<Zadanie> zadania = new ArrayList<>();

    public static void main(String[] args) {
        launch(args);
    }

    void zainicjujWykres(){
        CategoryAxis xAxis = new CategoryAxis();

        xAxis.setCategories(FXCollections.observableArrayList(Arrays.asList("M4", "M3", "M2", "M1")));
        xAxis.setLabel("Maszyny (M)");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("jednostka czasu (t)");

        stackedBarChart = new StackedBarChart<>(yAxis, xAxis);
        stackedBarChart.setTitle("Szeregowanie zadan algorytmem LWR");
        stackedBarChart.setLegendVisible(false);
    }

    @Override
    public void start(Stage stage) throws IOException {
        zainicjujWykres();

        TableView tabela = stworzTabele();
        ObservableList<ZadanieWTabeli> zadaniaWTabeli = FXCollections.observableArrayList(
                new ZadanieWTabeli(1, 0, "M1(10) M2(20) M3(35)", "red"),
                new ZadanieWTabeli(2, 0, "M2(25) M1(20) M3(30) M4(15)", "green"),
                new ZadanieWTabeli(3, 20, "M2(10) M4(10)", "blue"),
                new ZadanieWTabeli(4, 30, "M1(15) M3(10) M4(20)", "yellow"));
        tabela.setItems(zadaniaWTabeli);

        mapaKolorow.put("zad1", "red");
        mapaKolorow.put("zad2", "green");
        mapaKolorow.put("zad3", "blue");
        mapaKolorow.put("zad4", "yellow");

        Label dodajZadanie = new Label("Dodaj zadanie");
        Label rLabel = new Label("R");
        TextField rPoleTekstowe = new TextField();
        Label marszrutaLabel = new Label("Marszruta. Format: Mi(pi) Mi+1(pi+1) ... Mn(pi+n)");
        TextField marszrutaPoleTekstowe = new TextField();
        Button dodaj = new Button("Dodaj");
        Button licz = new Button("Licz");
        licz.setOnAction(e -> {
            zadania.clear();

                try {
                    for (ZadanieWTabeli zad : zadaniaWTabeli) {
                        var elementy = this.ParsujMarszruteTekst(zad.getMarszruta());
                        var nrZadania = "zad" + (zadania.size() + 1);
                        zadania.add(new Zadanie(nrZadania, zad.getR(), elementy));
                    }

                    this.liczWszystko(zadania);
                }
                catch (Exception ex)
                {
                    Label wiadomosc = new Label("Błąd nie udało się odczytać danych wejściowych. " + ex.getMessage());
                    Popup popup = new Popup();
                    popup.getContent().add(wiadomosc);
                    popup.show(stage);
                }
        });

        dodaj.setOnAction(e -> {
            try {
                if (marszrutaPoleTekstowe.getText().isBlank()){
                    throw new Exception("Puste pole");
                }
                var wartosc = Integer.parseInt(rPoleTekstowe.getText());
                var numerZadania = zadaniaWTabeli.size() + 1;
                String klucz = "zad" + numerZadania;
                String kolor = losujKolor();
                mapaKolorow.put(klucz, kolor);

                zadaniaWTabeli.add(new ZadanieWTabeli(numerZadania, wartosc, marszrutaPoleTekstowe.getText(), kolor));

            } catch(Exception ex) {
                Label wiadomosc = new Label("Błąd: r nie jest liczbą lub marszruta pusta!");
                Popup popup = new Popup();
                popup.getContent().add(wiadomosc);
                popup.show(stage);

                rPoleTekstowe.setText("");
                marszrutaPoleTekstowe.setText("");
            }
        });

        Button wyczyscWszystko = new Button("Wyczyć");
        wyczyscWszystko.setOnAction(e -> {
            zadaniaWTabeli.clear();
        });

        VBox przyciskiTabelkaVBox = new VBox(
                dodajZadanie,
                rLabel,
                rPoleTekstowe,
                marszrutaLabel,
                marszrutaPoleTekstowe,
                dodaj,
                wyczyscWszystko,
                licz);

        HBox legendaTabelka = new HBox(tabela, przyciskiTabelkaVBox);

        VBox vbox = new VBox(stackedBarChart, legendaTabelka);

        Scene scene = new Scene(vbox, 800, 600);
        stage.setTitle("Szeregowanie zadan algorytmem LWR");
        stage.setScene(scene);
        stage.show();
    }

    public void liczWszystko(List<Zadanie> zadania) {
        List<XYChart.Series<Number, String>> listaSerii = new ArrayList<>();

        Manager manager = new Manager(zadania);
        manager.start();

        Map<String, List<LogMaszyny>> pogrupowaneLogi = manager
                .maszyny
                .stream()
                .flatMap(m -> m.logiMaszyny.stream())
                .collect(Collectors.groupingBy(logm -> logm.maszynaId))
                .entrySet().stream().sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (poprzednia, nowa) -> poprzednia, LinkedHashMap::new
                ));

        for (Map.Entry<String, List<LogMaszyny>> para : pogrupowaneLogi.entrySet()){ //idzie po grupach maszyn (pogrupowane logi maszyn) i tworzy dane do wykresu dla kazdej maszyny
            var lista = stworzListeSerii(para.getValue(), para.getKey());

            listaSerii.addAll(lista);
        }

        stackedBarChart.getData().setAll(listaSerii);

        pokolorujWykres(stackedBarChart);
    }

    public List<Element> ParsujMarszruteTekst(String tekst) throws IOException {
        List<Element> elementy = new LinkedList<Element>();

        var elementyText = tekst.split(" ");
        for (String element: elementyText) {
            String maszynaId = element.substring(element.indexOf("M"), element.indexOf("("));
            String czasTrwaniaTekst = element.substring(element.indexOf("(") + 1, element.indexOf(")"));

            var czasTrwania = Integer.parseInt(czasTrwaniaTekst);

            elementy.add(new Element(element, maszynaId, czasTrwania));
        }

        return elementy;
    }

    public TableView<ZadanieWTabeli> stworzTabele() {
        TableView<ZadanieWTabeli> tabela = new TableView<>();
        tabela.setEditable(true);
        tabela.setPrefWidth(500);

        TableColumn<ZadanieWTabeli,String> kolorCol = new TableColumn<>("Kolor");

        TableColumn<ZadanieWTabeli,String> nrCol = new TableColumn<>("Nr zadania");
        nrCol.setCellValueFactory(new PropertyValueFactory("nr"));

        TableColumn<ZadanieWTabeli,String> rCol = new TableColumn<>("r");
        rCol.setCellValueFactory(new PropertyValueFactory("r"));

        TableColumn<ZadanieWTabeli,String> marszrutaCol = new TableColumn<>("Marszruta");
        marszrutaCol.setCellValueFactory(new PropertyValueFactory("marszruta"));

        kolorCol.setCellFactory(d -> new LegendaKomorka());

        tabela.getColumns().addAll(kolorCol, nrCol, rCol, marszrutaCol);

        return tabela;
    }

    public void pokolorujWykres(StackedBarChart<Number, String> wykres) {
        for (XYChart.Series<Number, String> series : wykres.getData()) {
            var name = series.getName();

            if (name.equals("Wolne")) {
                for(XYChart.Data data : series.getData())
                {
                    data.getNode().setStyle("-fx-bar-fill: transparent;");
                }
            }

            String color = mapaKolorow.get(name);
            for(XYChart.Data data : series.getData())
            {
                data.getNode().setStyle("-fx-bar-fill: " + color + "; ");
            }
        }
    }

    public static String losujKolor()
    {
        var red = new Random().nextInt(255);
        var blue = new Random().nextInt(255);
        var green = new Random().nextInt(255);

        return String.format( "#%02X%02X%02X", red, blue, green);
    }

    // Zamienia element(operacje) maszyny na serie danych do wykresu = log to informacja na emat pracy maszyny nad danym elementem(operacja) zadania
    public List<XYChart.Series<Number, String>> stworzListeSerii(List<LogMaszyny> logi, String maszynaId) {
        var zbiory = new ArrayList<XYChart.Series<Number, String>>();

        // Jak jest nie zero pierwsze to dodaj przerwe na poczatku
        LogMaszyny pierwszyLog = logi.get(0);
        if (pierwszyLog.czasRozpoczecia != 0)
        {
            var seriaPrzerwa = dodajeWolne(pierwszyLog.czasRozpoczecia, maszynaId);
            zbiory.add(seriaPrzerwa);
        }

        for (int i = 0; i < logi.size(); i++) {
            LogMaszyny logMaszyny = logi.get(i);
            Integer czasRozpoczecia = logMaszyny.czasRozpoczecia;
            Integer czasZakonczenia = logMaszyny.czasZakonczenia;

            Integer roznica = czasZakonczenia - czasRozpoczecia;

            XYChart.Series<Number, String> seria = new XYChart.Series<>();
            seria.setName(logMaszyny.zadanieId);
            seria.getData().add(new XYChart.Data<>(roznica, maszynaId));
            zbiory.add(seria);

            // czy to ostatni log
            if (i < logi.size() - 1) {
                // sprawdz czy jest przerwa w pracy miedzy następującymi po sobie Logami Maszyn
                LogMaszyny ostatniLogMaszyny = logi.get(i + 1);
                Integer czasRozpoczeciaKolejnej = ostatniLogMaszyny.czasRozpoczecia;
                Integer roznicaPrzerwa = czasRozpoczeciaKolejnej - czasZakonczenia;
                if (roznicaPrzerwa > 0) {
                    var seriaPrzerwa = dodajeWolne(roznicaPrzerwa, maszynaId);
                    zbiory.add(seriaPrzerwa);
                }
            }
        }

        return zbiory;
    }

    public XYChart.Series<Number, String> dodajeWolne(Integer wartosc, String maszynaId)
    {
        XYChart.Series<Number, String> seriaPrzerwa = new XYChart.Series<>();
        seriaPrzerwa.setName("Wolne");
        seriaPrzerwa.getData().add(new XYChart.Data<>(wartosc, maszynaId));

        return seriaPrzerwa;
    }
}