public class LogMaszyny {
    public String maszynaId; // potrzebne do grupowania logow w main
    public String zadanieId;
    public int czasRozpoczecia; //rzeczywisty czas rozpoczecia - s
    public int czasZakonczenia; //rzeczywisty czas zakonczenia - c

    public LogMaszyny(String maszynaId, String zadanieId, int czasRozpoczecia, int czasZakonczenia){
        this.maszynaId = maszynaId;
        this.zadanieId = zadanieId;
        this.czasRozpoczecia = czasRozpoczecia;
        this.czasZakonczenia = czasZakonczenia;
    }

    public String getZadanieId() {
        return zadanieId;
    }

    public int getCzasRozpoczecia() {
        return czasRozpoczecia;
    }

    public int getCzasZakonczenia() {
        return czasZakonczenia;
    }

    @Override
    public String toString() {
        return "LogMaszyny{" +
                "maszynaId='" + maszynaId + '\'' +
                ", zadanieId='" + zadanieId + '\'' +
                ", czasRozpoczecia=" + czasRozpoczecia +
                ", czasZakonczenia=" + czasZakonczenia +
                '}';
    }
}
