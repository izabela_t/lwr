import javafx.scene.control.TableCell;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

class LegendaKomorka extends TableCell<ZadanieWTabeli, String> {
    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (getTableRow() != null && getTableRow().getItem() != null)
        {
            System.out.println(item);

            System.out.println(getTableRow().getItem());
            Rectangle rect = new Rectangle(100, 20);
            rect.setFill(Color.web(getTableRow().getItem().getKolor()));

            System.out.println(getTableRow().getItem().getKolor());
            setGraphic(rect);
        } else {
            setGraphic(null);
        }
    }
}
