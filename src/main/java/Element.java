public class Element {
    public enum Stan {
        OCZEKUJE, W_TRAKCIE
    }

    public String id;
    public String maszynaId;
    public Integer czasTrwania;
    public Stan stan = Stan.OCZEKUJE;
    public Zadanie zadanie;

    public Element(String id, String maszynaId, Integer czasTrwania){
        this.id = id;
        this.maszynaId = maszynaId;
        this.czasTrwania = czasTrwania;
    }

    public void usunZMarszruty(){
        zadanie.marszruta.remove(this);
    }

    @Override
    public String toString() {
        return "Element{" +
                "id='" + id + '\'' +
                ", maszynaId='" + maszynaId + '\'' +
                ", czasTrwania=" + czasTrwania +
                ", stan=" + stan +
                '}';
    }
}
