import java.util.*;
import java.util.stream.Collectors;

public class Manager {
    List<Maszyna> maszyny = new ArrayList<>();
    List<Zadanie> zadania;

    //tworzenie listy maszyn i listy zadan
    public Manager(List<Zadanie> zadania){
        Maszyna m1 = new Maszyna("M1");
        Maszyna m2 = new Maszyna("M2");
        Maszyna m3 = new Maszyna("M3");
        Maszyna m4 = new Maszyna("M4");

        maszyny.add(m1);
        maszyny.add(m2);
        maszyny.add(m3);
        maszyny.add(m4);

        this.zadania = zadania;
    }

    public void start(){
        Integer i = 0;
        while (!czyStop()){
            uaktualnijPraceProgramu(i); // i to jest dana jednostka czasu całkowitego czasu programu
            i++;
        }
    }

    public boolean czyStop(){
        return zadania.stream().filter(z -> !z.marszruta.isEmpty()).count() == 0;
    }

    public void uaktualnijPraceProgramu(Integer jednostkaCzasu){
        uaktualnijMaszyny();
        zwolnijMaszyny(jednostkaCzasu);

        for (Maszyna m : maszyny){
            if(m.stan == Maszyna.Stan.WOLNA) {
                List<Element> dostepneElementyZadan = zbierzOczekujaceElementyZadan(jednostkaCzasu, m.id);

                if (dostepneElementyZadan.size() > 1) { //konflikt
                    Element elementLWR = dostepneElementyZadan
                            .stream()
                            .sorted(Comparator.comparing(e -> e.zadanie.liczPozostalyCzas())) //wywoluje kazde zadanie i liczy pozostaly czas do jego zakonczenia
                            .findFirst()
                            .get();
                    System.out.println(String.format("KONFLIKT Maszyna %s wybiera zadanie %s z czasem do konca %d", m.id, elementLWR.zadanie.id, elementLWR.zadanie.liczPozostalyCzas()));
                    m.start(elementLWR, jednostkaCzasu); // rozwiazanie konfliktu
                } else if (dostepneElementyZadan.size() == 1) {
                    m.start(dostepneElementyZadan.get(0), jednostkaCzasu);
                }
            }
        }
    }

    public void uaktualnijMaszyny(){
        for (Maszyna m : maszyny){
            m.uaktualnij();
        }
    }

    public void zwolnijMaszyny(Integer jednostkaCzasu){
        for (Maszyna m : maszyny){
            if(m.stan == Maszyna.Stan.ZAKONCZONO){
                System.out.println(String.format("Maszyna %s zakonczyla element %s o czasie %d", m.id, m.element.id, jednostkaCzasu));
                m.zwolnij();
            }
        }
    }

    public List<Element> zbierzOczekujaceElementyZadan(Integer jednostkaCzasu, String maszynaId){ //zbiera elementy ktore dana maszyna powinna wiac w danej jednostce czasu
           return zadania
                   .stream() //sluzy do zbierania zadan jakie moze przyjac maszyna w danej jednostce czasu
                   .filter(z -> z.r <= jednostkaCzasu)
                   .filter(z -> !z.marszruta.isEmpty())
                   .filter(z -> z.marszruta.get(0).stan == Element.Stan.OCZEKUJE)
                   .filter(z -> z.marszruta.get(0).maszynaId.equals(maszynaId))
                   .map(z -> z.marszruta.get(0))
                   .collect(Collectors.toList());
    }
}
