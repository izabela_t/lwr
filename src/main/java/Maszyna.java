import java.util.ArrayList;
import java.util.List;

public class Maszyna {
    public enum Stan {
        ZAKONCZONO, W_TRAKCIE, WOLNA
    }

    public String id;
    public Integer aktualnyCzasWykonywania = 0; //czas pobytu zadania w sytemie - F
    public Stan stan = Stan.WOLNA;
    public List<LogMaszyny> logiMaszyny = new ArrayList<>();
    public Element element; //element nad ktorym aktualnie pracuje maszyna

    public Maszyna(String id){
        this.id = id;
    }

    public void start(Element element, Integer czasWProgramie){ //jednostka czasu programu jaka aktualnie trwa
        this.element = element;
        this.element.stan = Element.Stan.W_TRAKCIE;
        stan = Stan.W_TRAKCIE;

        LogMaszyny logMaszyny =
                new LogMaszyny(id,
                        element.zadanie.id,
                        czasWProgramie,
                        czasWProgramie + element.czasTrwania);

        logiMaszyny.add(logMaszyny);
        System.out.println(String.format("Maszyna %s, zaczyna element %s, o czasie %d", id, element.id, czasWProgramie));
    }

    public void uaktualnij(){
        if(stan == Stan.W_TRAKCIE){
            aktualnyCzasWykonywania++;
        }

        if(element != null && element.czasTrwania == aktualnyCzasWykonywania){
            stan = Stan.ZAKONCZONO;
        }
    }

    public void zwolnij(){
        element.usunZMarszruty();
        aktualnyCzasWykonywania = 0; //bo maszyna zakoncyla jednostke pracy zadania
        stan = Stan.WOLNA;
    }

}
